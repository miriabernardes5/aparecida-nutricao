var botaoAdicionar = document.querySelector("#buscar-pacientes");

botaoAdicionar.addEventListener("click", function(){
    /* Instancia um objeto de requisição http */
    var xhr = new XMLHttpRequest(); 

    /* Abre uma conexão, realizar uma requisição http(GET, POST, DELETE, PUT) do tipo GET(retorna um resultado) 
    passa por parâmetro para onde será feita a requisição*/
    xhr.open("GET", " https://api-pacientes.herokuapp.com/pacientes");

    /*  Exibir os dados após o envio da requisição, para tal é necessário que 
    seja adicionado um listener especifico que escutará quando a requisição terminar e
    a resposta estiver carregada! */
    xhr.addEventListener("load", function(){
        if (xhr.status == 200){
            var resposta = xhr.responseText;
            /* responseText é o responsável para acessar os dados da resposta da requisição. */
            var pacientes = JSON.parse(resposta); /* Transforma a resposta da requição em um arq. JSON */
            pacientes.forEach(function(paciente){ /* Iterar o arq JSON e adicionar object a object na tabela de pacientes. */
                adicionarPacientes(paciente);
            });
        }else{
            var erroAjax = document.querySelector("#erro-ajax");
            erroAjax.classList.remove("invisivel");
        }
    });
    /* Método que realiza de fato o envio da requisição */
    xhr.send();
    
});

/* 
=============================================== Considerações Finais ========================================================
A técnica utilizada nessa aula é conhecida como AJAX, essa maneira de fazer uma requisição de forma assíncrona com JavaScript.
É uma requisição assíncrona porque não está parando o fluxo do código, ou seja, no momento em que a requisição é feita, 
a execução continua normalmente.
Durante esse processo de busca de pacientes no servidor externo, é possível excluir e adicionar pacientes.

*/