
var botaoAdicionar = document.querySelector("#adicionar-paciente");
botaoAdicionar.addEventListener("click", function (event) {
    event.preventDefault();

    var form = document.querySelector("#form-adiciona");
    var paciente = obtemPaciente(form); 

    if (!validaPaciente(paciente)) {
        alert("Dados sobre peso/altura são inválidos!")
    } else {
       adicionarPacientes(paciente);
        form.reset();
        alert("Novo paciente foi adicionado a lista!");
    }
});

/* Extraindo informações do paciente do form */
var form = document.querySelector("#form-adiciona");
function obtemPaciente(form) {
    var paciente = {
        nome: form.nome.value,
        peso: form.peso.value,
        altura: form.altura.value,
        gordura: form.gordura.value,
        imc: calculaIMC(form.peso.value, form.altura.value)
    }
    return paciente;
}

function adicionarPacientes(paciente){
    var tabela = document.querySelector("#tabela-pacientes");
    var pacienteTr = criaPacientes(paciente);
    tabela.appendChild(pacienteTr);
}
/* Criando paciente */
function criaPacientes(paciente) {
    var pacienteTr = document.createElement("tr");
    pacienteTr.classList.add("paciente");
    pacienteTr.appendChild(montaTd(paciente.nome, "info-nome"));
    pacienteTr.appendChild(montaTd(paciente.peso, "info-peso"));
    pacienteTr.appendChild(montaTd(paciente.altura, "info-altura"));
    pacienteTr.appendChild(montaTd(paciente.gordura, "info-gordura"));
    pacienteTr.appendChild(montaTd(paciente.imc, "info-peso"));

    return pacienteTr;
}

function montaTd(dado, classe) {
    var td = document.createElement("td");
    td.textContent = dado;
    td.classList.add(classe);
    return td;
}

function validaPaciente(paciente) {
    if (validaPeso(paciente.peso) && validaAltura(paciente.altura)) {
        return true;
    } else {
        return false;
    }
}

