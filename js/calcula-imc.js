var paciente = document.querySelectorAll(".paciente");



for (var i = 0; i < paciente.length; i++) {
    var tdAltura = paciente[i].querySelector(".info-altura");
    var tdPeso = paciente[i].querySelector(".info-peso");
    var tdImc = paciente[i].querySelector(".info-imc");

    var altura = tdAltura.textContent;
    var peso = tdPeso.textContent;

    var alturaEhValida = validaAltura(altura);
    var pesoEhValido = validaPeso(peso);

    if (!pesoEhValido) {
        console.log("Peso inválido!");
        tdPeso.textContent = "Peso inválido!";
        pesoEhValido = false;
        paciente[i].classList.add("paciente-invalido");
    }

    if (!alturaEhValida) {
        console.log("Altura inválida!");
        tdAltura.textContent = "Altura inválida!";
        alturaEhValida = false;
        paciente[i].classList.add("paciente-invalido");
    }

    if (alturaEhValida && pesoEhValido) {
        var imc = calculaIMC(peso, altura);
        tdImc.textContent = imc;
        var btnSalvar = document.querySelector("#adicionar-paciente");
        btnSalvar.addEventListener("click", function () {
        });
    } else {
        tdImc.textContent = "Altura e/ou peso inválidos!"
    }
}


function calculaIMC(peso, altura) {
    var imc = 0;
    imc = peso / (altura * altura);
    return imc.toFixed(2);
}

function validaPeso(peso){
    if (peso >= 0 && peso <= 1000) {
        return true;
    }else{
        return false;
    }
}

function validaAltura(altura){
    if(altura >= 0 && altura <= 3){
        return true;
    }else{
        return false;
    }
}