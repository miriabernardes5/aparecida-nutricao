module.exports = function(grunt) {
    
    grunt.initConfig({
      browserSync: {
        public: {
            bsFiles: {
                src: ['public/**/*']
            },
            options: {
                server: {
                    baseDir: "public"
                }
            }
        }
      }
    });
    
    grunt.registerTask('serve', ["browserSync"]);
    grunt.loadNpmTasks('grunt-browser-sync');
}